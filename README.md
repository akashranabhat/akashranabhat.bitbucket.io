# PMC design 

## git cheatsheet
```bash 
# create branch 
git checkout -b <branch_name>

# change branch 
git checkout <branch_name>

# add files on staging
git add .

# commit changes
git commit -m "message"

# push to origin
git push origin <branch_name>

# pull code from main
git pull origin main

# update branch
git rebase main
```