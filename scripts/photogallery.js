const imageContainer = document.getElementById('image-container');
const col1 = document.getElementById('col-1');
const col2 = document.getElementById('col-2');
const col3 = document.getElementById('col-3');
fetch('http://patancampus.herokuapp.com/api/gallery')
    .then((response)=> response.json())
    .then(data => {
        const items = data.data.galley;
        console.log(data.data.galley);
        for(let i=0; i <= items.length; i+=3){
            const image1 = document.createElement('a');
            image1.innerHTML = `
                <div class="image-text">
                    <img src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain2.webp"
                        class="w-100 shadow-1-strong rounded mb-4" alt="Mountains in the Clouds" />
                    <div class="text-top">Wintry ${items[i].title}</div>
            `;
            image1.href = "/gallary.html?id="+items[i].id;
            col1.appendChild(image1);
            const image2 = document.createElement('a');
            image2.innerHTML = `
            <div class="image-text">
                <img src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
                    class="w-100 shadow-1-strong rounded mb-4" alt="Boat on Calm Water" />
                <div class="text-top">${items[i+1].title}</div>
            `;
            image2.href = "/gallary.html?id="+items[i+1].id;
            col2.appendChild(image2);
            const image3 = document.createElement('a');
            image3.innerHTML = 
            `
        <div class="image-text">
            <img src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain3.webp"
                class="w-100 shadow-1-strong rounded mb-4" alt="Yosemite National Park" />
            <div class="text-top">${items[i+2].title}</div>
            `;
            image3.href = "/gallary.html?id="+items[i].id;
            col3.appendChild(image3);
        }
    })