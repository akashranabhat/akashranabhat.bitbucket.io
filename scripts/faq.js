const categoriesList = document.getElementById('categories-list');
const accordion = document.getElementById('accordionExample');
let faqItems = []
let faqCategories = [];
fetch('http://patancampus.herokuapp.com/api/faq')
    .then(response  => response.json())
    .then(data => {
        faqItems = data.data.faq;
        faqCategories = data.data.faqcategories;
        let li = document.createElement('li');
        li.className = "list-group-item active";
        li.innerText = "Categories";
        li.addEventListener('click', ()=>{
            const active  = document.querySelector('.list-group-item.active');
                active.classList.remove("active");
                li.classList.add('active');
                showAll();
        })
        categoriesList.appendChild(li);
        faqCategories.forEach((item)=>{
            let li = document.createElement('li');
            li.className = "list-group-item";
            li.innerText = item.title;
            li.addEventListener('click', ()=>{
                const active  = document.querySelector('.list-group-item.active');
                active.classList.remove("active");
                li.classList.add('active');
                filter(item.id);
                
            })
            categoriesList.appendChild(li);
        });
        showAll();
        // add display true to all items 
    })
    .catch(error=>{
        accordion.innerText = "Error loading Page";
    });

function filter(id){
    faqItems.map((item)=>{
        if(item.faq_category_id === id){
            item.display = true;
        }
        else{
            item.display = false;
        }
    })
    displayFaq();
}

function showAll(){
    faqItems.map((item)=>{
        item.display = true;
    });
    displayFaq();
}

function displayFaq(){
    accordion.innerHTML = "";
    faqItems.forEach((item, index)=>{
        if(item.display === false){
            return ;
        }
        const accordianItem = document.createElement('div');
        accordianItem.className = "accordion-item";
        accordianItem.innerHTML = `
        <h2 class="accordion-header" id="heading-${item.id}">
          <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-${item.id}" aria-expanded="true" aria-controls="collapse-${item.id}">
            ${item.question}
          </button>
        </h2>
        <div id="collapse-${item.id}" class="accordion-collapse collapse" aria-labelledby="heading-${item.id}" data-bs-parent="#accordionExample">
          <div class="accordion-body">
            ${item.answer}
          </div>
        </div>`;
        accordion.appendChild(accordianItem);
    })
}