const params = new URLSearchParams(window.location.search);
const gallery = document.getElementById('gallery-row');
if(params.has('id')){
    const id  = params.get('id');
    fetch(`https://patancampus.herokuapp.com/api/galleries/${id}`)
        .then(response => response.json())
        .then(data =>{
            console.log(data.data[0].image)
            const items = data.data[0].image.split(" ");
            console.log(items);
            items.forEach((item)=>{
                const col = document.createElement('a');
                col.className = "col-sm-6 col-md-4 col-lg-3";
                col.innerHTML = `
                <a href="https://cdn.pixabay.com/photo/2017/04/08/22/26/buddhism-2214532__480.jpg">
                    <img class="img-fluid" src="https://cdn.pixabay.com/photo/2017/04/08/22/26/buddhism-2214532__480.jpg" />
                </a>
                `;
                gallery.appendChild(col);
            })
        })
}else{
    console.log('Nothing to show');
}