let topNav = document.getElementById("sticky-nav");
function openSearch() {
  document.getElementById("myOverlay").style.display = "block";
  topNav.className = "";
  document.body.style.overflow = "hidden";
}

// Close the full screen search box
function closeSearch() {
  document.getElementById("myOverlay").style.display = "none";
  topNav.className = "sticky-top";
  document.body.style.overflow = "auto";
}
