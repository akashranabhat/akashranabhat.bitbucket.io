let components = {
    footer:{
        url: 'footer.html',
        dom: document.getElementById("footer")
    },
    header: {
        url: 'header.html',
        dom: document.getElementById("header")
    },
    headerTop: {
        url: 'header-top.html',
        dom: document.getElementById('header-top')
    },
    headerNav: {
        url: 'header-nav.html',
        dom: document.getElementById('header-nav')
    },
    carousel: {
        url: 'carousel.html',
        dom: document.getElementById('my-carousel')
    },
    ticker: {
        url: 'ticker.html',
        dom: document.getElementById('ticker')
    },
    notices: {
        url: 'notice.html',
        dom: document.getElementById('notices')
    },
    overlay: {
        url: 'overlay.html',
        dom: document.getElementById('overlay')
    },
    exam_notice: {
        url: 'exam-notice.html',
        dom: document.getElementById('exam-notice')
    },
    programSlides: {
        url: 'program-slides.html',
        dom: document.getElementById('program-slides')
    },
    pagination: {
        url: 'pagination.html',
        dom: document.getElementById('pagination')
    }
}
for(const component in components){
    fetchHTML(components[component]);
}
function fetchHTML(component){
    fetch(`/components/${component.url}`)
    .then(function(response){
        return response.text();
    })
    .then(function(html){
        component.dom.innerHTML = html;
    })
    .catch(function(error){
        console.log(component);
        console.log(error);
    })
}


// for department
function fetchHTML(component){
    fetch(`./components/${component.url}`)
        .then(function(response){
            return response.text();
        })
        .then(function(html){
            component.dom.innerHTML = html;
        })
        .catch(function(error){
            console.log(component);
            console.log(error);
        })
}




